import random
import json


class WeatherService:
    """A simple weather service that returns the weather for a location"""
    def __init__(self):
        self.weatherChoices = [
            {"id": 212, "main": "Thunderstorm", "description": "heavy thunderstorm"},
            {"id": 314, "Drizzle": "Rain", "description": "heavy shower rain and drizzle"},
            {"id": 511, "main": "Rain", "description": "freezing rain"},
            {"id": 616, "main": "Snow", "description": "Rain and snow"},
            {"id": 761, "main": "Dust", "description": "dust"},
            {"id": 781, "main": "Tornado", "description": "tornado"},
            {"id": 804, "main": "Clouds", "description": "overcast clouds"}
        ]

    def forecast(self, zipcode):
        """Returns the weather forecast for the given zipcode"""
        return json.dumps(self.weatherChoices[random.randint(0, len(self.weatherChoices))])

