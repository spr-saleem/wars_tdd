Simple Web-Aware Routing System project in Python using pytest

© SPR Inc. 2020. Distributed under the terms and conditions described in the LICENSE file.

# Goal

Develop a routing system that uses the `WeatherService` to determine when and how to reroute.

Here are a few tips on how to start.

1. Write a `reroute()` function using TDD. This function should reroute when there is inclement weather at a 
given location,
2. Mock the `WeatherService` to help you write the `reroute` function.
3. Build up the `reroute()` function from *very simple* tests to more complicated scenarios.
4. Replace the `WeatherService` with the equivalent service from https://openweathermap.org/api and verify
that it still works as expected. (You may have to do some refactoring; as the API's response is more expansive
than the original `WeatherService`'s.)

# Running

Run `pytest` from the command line to run all the tests.

