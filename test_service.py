from unittest.mock import MagicMock
from service import WeatherService


def test_service():
    mockService = MagicMock()
    mockService.forecast.return_value = '{"foo": "bar"}'
    assert serviceWrapper(mockService, "12345") == '{"FOO": "BAR"}'


def serviceWrapper(service, zipcode):
    return service.forecast(zipcode).upper()
